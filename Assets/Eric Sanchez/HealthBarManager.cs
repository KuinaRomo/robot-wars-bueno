﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarManager : MonoBehaviour {
    public List<GameObject> robotsA, robotsB;
    GameManager gameManagerScr;
    
    // Use this for initialization
    void Start () {
        
        gameManagerScr = GameObject.Find("GameManager").GetComponent<GameManager>();
       
        InitArrays();
	}

    void InitArrays()
    {
        GameObject[] arr = GameObject.FindGameObjectsWithTag("robotA");
        GameObject[] arr2 = GameObject.FindGameObjectsWithTag("robotB");
        robotsA.AddRange(arr);
        robotsA.AddRange(arr2);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void UpdateHealthBar()
    {
        
    }
}
