﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveAndPlay : MonoBehaviour {
    ComponentController.Component[] robots;

    [Header("Configuration for each robot")]
    [SerializeField] GameObject [] configuration;

    MusicControler musicManager;

    public AsembleRobot [] asR;

  

    Text errorLog;
   
    [SerializeField]Button saveAndPlay;



    // Use this for initialization
    void Start () {
        saveAndPlay = GetComponent<Button>();
        saveAndPlay.onClick.AddListener(delegate { SaveRobots(); });

        musicManager = GameObject.Find("MusicController").GetComponent<MusicControler>();

        errorLog = GameObject.Find("GenericCanvas").transform.Find("ErrorLogText").GetComponent<Text>();

    }
    void DontBuild()
    {
        errorLog.text += "\nYou need to change the type of body armor!!";
    }


	// Update is called once per frame
	void SaveRobots () {

        asR = new AsembleRobot[configuration.Length];
        robots = new ComponentController.Component[configuration.Length];
        for (int i = 0; i < configuration.Length; i++)
            asR[i] = configuration[i].GetComponent<AsembleRobot>();

        for (int i = 0; i < asR.Length; i++)
        {
            if (asR[i].cantBuild)
            {
                DontBuild();
                return;
            }
            else
                robots[i] = asR[i].finalComponent;
        }
        
        gameObject.transform.parent = null;
        DontDestroyOnLoad(gameObject);

        musicManager.audioSource[0].Stop();

        musicManager.audioSource[0].clip = musicManager.Music.audioClips[6];
        musicManager.audioSource[0].Play();

        SceneManager.LoadScene(2);
    }
   
}

/////!! Aqui empieza lo mio 
////vida, pasos, defensa, recursos, headType, torsoType, legsType, weapon, diagonalAttack, userRobot

//SaveAndPlay sap;

//sap = GameObject.Find("Save&Play").GetComponent<SaveAndPlay>();

//        for (int i = 0; i<sap.asR.Length; i++)
//        {
//            ComponentController.Component comp = sap.asR[i].finalComponent;
//createPlantilla(comp.armor, comp.weight, 0, comp.resourcesCost, comp.typeRobot[0], comp.typeRobot[1], comp.typeRobot[2], comp.robotWeapon, 1, 1, true, true);
//        }

//        //!! Aqui acaba