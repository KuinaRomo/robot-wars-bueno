﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HolderController : MonoBehaviour {

    [SerializeField] public string TypeOfHolder;
    public Transform currObject;
    ObjMenuHandler objScript;
    GameObject obj;
    // Use this for initialization
    void Start()
    {

    }

    public void setItem(Transform item)
    {
        if (currObject && currObject != item)
        {
            currObject.GetComponent<ObjMenuHandler>().resetPos();
        }
        currObject = item;
    }

    void OnCollisionEnter(Collision collision)
    {
        objScript = collision.gameObject.GetComponent<ObjMenuHandler>();
        Debug.Log("Obj = " + collision.gameObject);
        if (objScript.TypeOfModule == TypeOfHolder)
        {
            obj = collision.gameObject;
            Debug.Log("Obj = " + collision.gameObject);
        }
    }
}
