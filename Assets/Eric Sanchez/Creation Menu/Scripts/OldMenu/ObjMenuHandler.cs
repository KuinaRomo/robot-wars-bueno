﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjMenuHandler : MonoBehaviour {

    public
    string TypeOfModule;
    public GameObject[] holders;
    [SerializeField] private Vector2 initPos;
    Transform tf;

    // Use this for initialization

    public void resetPos()
    {
        tf.position = initPos;
    }
    GameObject[] ArrRemoveNull(GameObject[] arr)
    {
        List<GameObject> arrList = new List<GameObject>(arr);                            //Create a list with the same values that arr     
        arrList.RemoveAll(item => item == null);                                         //Remove all null values on the string
        arr = arrList.ToArray();                                                         //Convert the list to the array arr
        return arr;                                                                      //Return de array
    }
    GameObject[] DiscardHolders(GameObject[] arr)
    {
        GameObject[] auxArr = new GameObject[arr.Length];                               //Initialize auxArr with the arr lenght
        int j = 0;                                                                      //Initialize a second iterator
        for (int i = 0; i < arr.Length; i++)                                            //Iterate the array 'arr'
        {
            if (TypeOfModule == arr[i].GetComponent<HolderController>().TypeOfHolder)   //Take the holder where holder type is = to TypeOfModule
            {
                auxArr[j] = arr[i];                                                     //Equal the value from 'arr' to auxArr
                j++;
            }
        }
        auxArr = ArrRemoveNull(auxArr);                                                 //Remove all the null values on the auxArr
        return auxArr;
    }

    void printArray(GameObject[] x)                                                     //Iterate and debug.log the array x
    {
        for (int i = 0; i < x.Length; i++)
        {
            Debug.Log("Col " + i + " : " + x[i].name);
            Debug.Log("MaxSize Arr: " + x.Length);
        }
    }

    void Start()
    {
        tf = GetComponent<Transform>();
        initPos = tf.position;
        holders = GameObject.FindGameObjectsWithTag("Holder");                              //Take all game object with tag "Holder"
        holders = DiscardHolders(holders);                                                  //Discard the obj with different types 
        printArray(holders);
    }


    void Update()
    {

    }
}


