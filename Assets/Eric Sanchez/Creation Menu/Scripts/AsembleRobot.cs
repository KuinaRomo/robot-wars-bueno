﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class AsembleRobot : MonoBehaviour {

    public GameObject[]                  components;
    ComponentController[]                componentsScripts;
    ComponentController.Component[]      componentsObj;
    public ComponentController.Component finalComponent;


    public bool cantBuild;

    [Header("GameObjects de las barras de stats\n")]
    [SerializeField] GameObject armor;
    [SerializeField] GameObject attack;
    [SerializeField] GameObject weight;
    [SerializeField] GameObject resourceCost;

    Image imgArmor;
    Image imgAttack;
    Image imgWeight;
    Text costText;

    Text errorLog;

   
    ComponentController.Type restriction;
    ComponentController.Type robotType;

    void UpdateBars()
    {
        imgArmor.fillAmount         = (float)finalComponent.armor / 120;
        imgAttack.fillAmount        = (float)finalComponent.attack / 80;
        imgWeight.fillAmount        = (float) finalComponent.weight / 4;
        costText.text  = finalComponent.resourcesCost.ToString();
        
    }

    // Use this for initialization
    void Start ()
    {
        componentsScripts = new ComponentController[components.Length];
        componentsObj = new ComponentController.Component[componentsScripts.Length];
		for(int i = 0; i < components.Length; i++)
        {
            componentsScripts[i] = components[i].GetComponent<ComponentController>();
        }

        errorLog = GameObject.Find("GenericCanvas").transform.Find("ErrorLogText").GetComponent<Text>();

        armor        = transform.Find("StatsBars").transform.Find("ArmorBar").transform.Find("BarFill").gameObject;
        attack       = transform.Find("StatsBars").transform.Find("AttackBar").transform.Find("BarFill").gameObject;
        weight       = transform.Find("StatsBars").transform.Find("WeightBar").transform.Find("BarFill").gameObject;
        resourceCost = transform.Find("StatsBars").transform.Find("ResourceCostBar").transform.Find("CostText").gameObject;

        imgArmor        = armor.GetComponent<Image>();
        imgAttack       = attack.GetComponent<Image>();
        imgWeight       = weight.GetComponent<Image>();
        costText        = resourceCost.GetComponent<Text>();

        BuildRobot();
        UpdateBars();

    }

    public void BlockAssembling(bool active, ComponentController.Type type)
    {
        errorLog.gameObject.SetActive(active);

        errorLog.text = "This weapon is not compatible with the body type " + type;

        cantBuild = active;
    }
    public void BuildRobot()
    {
        finalComponent.armor         = 0;
        finalComponent.attack        = 0;
        finalComponent.weight        = 0;
        finalComponent.resourcesCost = 0;

        for (int i = 0; i < componentsScripts.Length; i++)
        {
            componentsObj[i] = componentsScripts[i].selected;
        }

        finalComponent.img = new Sprite[componentsObj.Length];
        finalComponent.typeRobot = new TypeRobot[componentsObj.Length];

        for (int i = 0; i < componentsObj.Length; i++)
        {
            finalComponent.armor         += componentsObj[i].armor;
            finalComponent.attack        += componentsObj[i].attack;
            finalComponent.weight        += componentsObj[i].weight;
            finalComponent.resourcesCost += componentsObj[i].resourcesCost;
           
            finalComponent.img[i]        = componentsObj[i].img[0];
            finalComponent.typeRobot[i]  = componentsObj[i].typeRobot[0];
            if (componentsObj[i].notCompatibleWith != restriction) restriction = componentsObj[i].notCompatibleWith;
            if (componentsObj[i].type   != ComponentController.Type.NULL) robotType = componentsObj[i].type;
        }


        if (restriction == robotType)
        {
            BlockAssembling(true, restriction);
        }
        else
        {
            BlockAssembling(false, restriction);
        }
            
        Debug.Log(restriction);
        UpdateBars();

    }

    
}
