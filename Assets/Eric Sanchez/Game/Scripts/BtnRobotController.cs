﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class BtnRobotController : MonoBehaviour {

    public Button btn;
	void Start () {
        btn = GetComponent<Button>();
        btn.onClick.AddListener(delegate { onClickButton(btn); });
    }
	
    void onClickButton(Button btn)
    {
        Debug.Log("Spawning robot from: " + gameObject.name);
    }
}
