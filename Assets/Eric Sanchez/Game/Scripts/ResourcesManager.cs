﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ResourcesManager : MonoBehaviour {

    //This script is used for the management of the resources bar behaviour
    //It will change the values of the resource bar and the size of the bar 
    public Text actualResourcesTxt, topResourcesTxt;
    public Image actualResourcesImg;
    public bool HelloWorld;
 
    
	// Use this for initialization
	void Start () {
        
	}

    void Update() {
               
    }

    public void renewResources(float topResources, float actualResources) {
        //We initialize the values of the bar with the actual values of the public floats

        topResourcesTxt.text = " / " + Mathf.Floor(topResources).ToString();
        actualResourcesTxt.text = Mathf.Floor(actualResources).ToString();

        //We initialize the bar fill amount with the percentage of the current resources

        actualResourcesImg.fillAmount = actualResources / topResources;
    }
}
