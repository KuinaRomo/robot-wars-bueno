﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraMovement : MonoBehaviour {

    public float panSpeed = 20f;
    public float zoomSpeed = 20f;
    public float panBorderThickness = 10f;
    public float lateralPanBorderThickness = 200f;
    public Vector2 panLimit, zoomLimit, minLimit;
    [SerializeField] GameObject cameraButton;
    [SerializeField] List<Sprite> cameraButtonImages;
    public bool moveCamera;
    
    Camera cam;
	
	void Start () {
        moveCamera = true;
        cam = GetComponent<Camera>();
	}
	
	
	void Update () {
        if (moveCamera) { 
            Vector3 pos = transform.position;

            if (Input.GetKey("w") || Input.mousePosition.y >= Screen.height - panBorderThickness)
            {
                pos.y += panSpeed * Time.deltaTime;
            }
            if (Input.GetKey("s") || Input.mousePosition.y <= panBorderThickness)
            {
                pos.y -= panSpeed * Time.deltaTime;
            }
            if(Input.GetKey("a") || Input.mousePosition.x <= lateralPanBorderThickness)
            {
                pos.x -= panSpeed * Time.deltaTime;
            }
            if (Input.GetKey("d") || Input.mousePosition.x >= Screen.width - lateralPanBorderThickness)
            {
                pos.x += panSpeed * Time.deltaTime;
            }
            float scroll = Input.GetAxis("Mouse ScrollWheel");
            cam.orthographicSize += scroll * zoomSpeed * 15f * Time.deltaTime;

            cam.orthographicSize = Mathf.Clamp(cam.orthographicSize, zoomLimit.x, zoomLimit.y);

            pos.x = Mathf.Clamp(pos.x, minLimit.x, panLimit.x);
            pos.y = Mathf.Clamp(pos.y, minLimit.y, panLimit.y);


            transform.position = pos;
        }
    }

    public void activeCamera() {
        if (moveCamera) {
            moveCamera = false;
            changeSprite();
        }

        else {
            moveCamera = true;
            changeSprite();
        }
    }

    public void changeSprite() {
        if (moveCamera) {
            cameraButton.GetComponent<Image>().sprite = cameraButtonImages[0];
        }
        else {
            cameraButton.GetComponent<Image>().sprite = cameraButtonImages[2];
        }
    }
    
}
