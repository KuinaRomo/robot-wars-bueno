﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseController : MonoBehaviour {
    private const string SceneName = "MainMenu";
    [SerializeField] GameObject foreground;
    [SerializeField] GameObject PauseCanvas;
    [SerializeField] GameObject Options;
    [SerializeField] detectClick detectClick;
    [SerializeField] GameManager gameManager;

	// Use this for initialization
	void Start () {
        Options = GameObject.Find("MusicController").GetComponent<MusicControler>().canvasOptions;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.P)) {
            foreground.SetActive(true);
            PauseCanvas.SetActive(true);
            detectClick.pause = true;
        }
	}

    public void returnGame() {
        foreground.SetActive(false);
        PauseCanvas.SetActive(false);
        detectClick.pause = false;
    }

    public void options() {
        PauseCanvas.SetActive(false);
        Options.SetActive(true);
    }

    public void returnPause() {
        PauseCanvas.SetActive(true);
        Options.SetActive(false);
    }

    public void exit() {
        GameObject.Find("MusicController").GetComponent<MusicControler>().audioSource[0].Stop();
        SceneManager.LoadScene(SceneName);
    }
}
