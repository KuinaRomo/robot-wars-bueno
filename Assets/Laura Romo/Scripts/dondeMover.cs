﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dondeMover : MonoBehaviour{

    //[SerializeField] private bool diagonal;
    public float totalLife;
    public float life;
    public int attackPower;
    public int numAttack;
    public int numCasillas;
    public int numFila;
    public int numCol;
    private int newNumFila;
    private int newNumCol;
    [SerializeField] private GameObject tablero;
    public GameObject moveImage;
    public GameObject attackImage;
    List<List<GameObject>> casillas = new List<List<GameObject>>();
    public List<string> objectsBlocked = new List<string>();
    //Mountains, blocks
    private GameObject robotCasilla;
    private Transform myTransform;
    //private int contador;
    [SerializeField] bool paintPath;
    private bool block;
    private bool blockX;
    private bool blockY;
    private bool printPath;
    public bool movido;
    public bool atacado;
    private GameObject GameManager;
    private Color color;
    private int casillasPintar;
    public int myId;
    public int miniRobot;

    public List<GameObject> pathRobot;
    private List<string> badTags;
    //private List<string> badTagsAttack;

    public void newId() {
        bool igual = true;
        myId = 0;
        while (igual) {
            igual = false;
            for(int i = 0; i < GameObject.FindGameObjectsWithTag("robotA").Length; i++) {
                GameObject temp = GameObject.FindGameObjectsWithTag("robotA")[i];
                if(temp.GetComponent<dondeMover>().myId == myId && temp != this.gameObject) {
                    myId++;
                    igual = true;
                }
            }

            for (int i = 0; i < GameObject.FindGameObjectsWithTag("robotB").Length; i++) {
                GameObject temp = GameObject.FindGameObjectsWithTag("robotB")[i];
                if (temp.GetComponent<dondeMover>().myId == myId && temp != this.gameObject) {
                    myId++;
                    igual = true;
                }
            }
        }
    }

    // Use this for initialization
    void Start()  {
        printPath = true;
        movido = false;
        atacado = false;
        //diagonal = true;
        //contador = 0;
        GameManager = GameObject.FindGameObjectWithTag("GameManager");
        //GameManager.GetComponent<GameManager>().robotID++;
        tablero = GameObject.Find("Tablero");
        myTransform = tablero.GetComponent<Transform>();
        //Haces una lista de una lista que contenga las casillas separadas por filas
        for (int i = 0; i < myTransform.childCount; i++) {
            casillas.Add(new List<GameObject>());
            for(int j = 0; j < myTransform.GetChild(i).childCount; j++){
                casillas[i].Add(myTransform.GetChild(i).GetChild(j).gameObject);
            }
        }
        /*objectsBlocked.Add("block");
        objectsBlocked.Add("ocupada");
        objectsBlocked.Add("factory");*/
    }

    // Update is called once per frame
    void Update() {
        
    }

    public void printMove(bool pintar, bool attack) {
        if (!attack) {
            color = new Color(0, 0, 0, 0);
            casillasPintar = numCasillas;
        }
        else {
            color = new Color(255, 0, 0, 0); ;
            casillasPintar = numAttack;
        }
        printPath = pintar;
        if (!movido || !atacado) {
            if (attack) {
                GameManager.GetComponent<GameManager>().atacando = true;
            }
            print(casillasPintar, true, 1, color, attack, printPath);
        }
    }

    public void print(int numCasillas, bool diagonal, int distance, Color color, bool attack, bool pintar) {
        int printCas = numCasillas;
        bool[] prints = {true, true, true, true, true, true, true, true};
        string nameCasilla;
        if (attack) {
            nameCasilla = "attack";
        }
        else {
            nameCasilla = "move";
        }
        for (int i = 1; i < numCasillas + 1; i++) {
            //Arriba / Abajo
            if (numFila + i < casillas.Count) {
                robotCasilla = casillas[numFila + i][numCol];
                if (checkPrint(robotCasilla) && prints[0] || prints[0] && attack && robotCasilla.tag == "factory" || prints[0] && attack && robotCasilla.tag == "ocupada") {
                    if(i >= distance) {
                        robotCasilla.tag = nameCasilla;
                        if (pintar) {
                            robotCasilla.GetComponent<SpriteRenderer>().color = color;
                            if (attack){
                                robotCasilla.transform.GetChild(1).gameObject.SetActive(true);
                            }
                            else {
                                robotCasilla.transform.GetChild(0).gameObject.SetActive(true);
                            }
                        }
                    }
                }
                else {
                    prints[0] = false;
                }
            }
            if (numFila - i >= 0) {
                robotCasilla = casillas[numFila - i][numCol];
                if (checkPrint(robotCasilla) && prints[1] || prints[1] && attack && robotCasilla.tag == "factory" || prints[0] && attack && robotCasilla.tag == "ocupada") {
                    if (i >= distance) {
                        robotCasilla.tag = nameCasilla;
                        if (pintar) {
                            robotCasilla.GetComponent<SpriteRenderer>().color = color;
                            if (attack) {
                                robotCasilla.transform.GetChild(1).gameObject.SetActive(true);
                            }
                            else {
                                robotCasilla.transform.GetChild(0).gameObject.SetActive(true);
                            }
                        }
                    }
                }
                else {
                    prints[1] = false;
                }
            }

            //Derecha / Izquierda
           if (numCol + i < casillas[numFila].Count) {
                robotCasilla = casillas[numFila][numCol + i];
                if (checkPrint(robotCasilla) && prints[2] || prints[2] && attack && robotCasilla.tag == "factory" || prints[0] && attack && robotCasilla.tag == "ocupada") {
                    if (i >= distance) {
                        robotCasilla.tag = nameCasilla;
                        if (pintar) {
                            robotCasilla.GetComponent<SpriteRenderer>().color = color;
                            if (attack) {
                                robotCasilla.transform.GetChild(1).gameObject.SetActive(true);
                            }
                            else {
                                robotCasilla.transform.GetChild(0).gameObject.SetActive(true);
                            }
                        }
                    }
                }
                else {
                    prints[2] = false;
                }
            }
            if (numCol - i >= 0) {
                robotCasilla = casillas[numFila][numCol - i];
                if (checkPrint(robotCasilla) && prints[3] || prints[3] && attack && robotCasilla.tag == "factory" || prints[0] && attack && robotCasilla.tag == "ocupada") {
                    if (i >= distance) {
                        robotCasilla.tag = nameCasilla;
                        if (pintar) {
                            robotCasilla.GetComponent<SpriteRenderer>().color = color;
                            if (attack) {
                                robotCasilla.transform.GetChild(1).gameObject.SetActive(true);
                            }
                            else {
                                robotCasilla.transform.GetChild(0).gameObject.SetActive(true);
                            }
                        }
                    }
                }
                else {
                    prints[3] = false;
                }
            }
        }

       for(int i = 0; i < numCasillas; i++) {
            for (int j = 0; j < printCas; j++){
                if (numFila + j < casillas.Count && numCol + i + 1 < casillas[numFila + j].Count) {
                    robotCasilla = casillas[numFila + j][numCol + i + 1];
                    if (checkPrint(robotCasilla) && prints[4] || prints[4] && attack && robotCasilla.tag == "factory" || prints[0] && attack && robotCasilla.tag == "ocupada") {
                        if (j >= distance - 1) {
                            robotCasilla.tag = nameCasilla;
                            if (pintar) {
                                robotCasilla.GetComponent<SpriteRenderer>().color = color;
                                if (attack) {
                                    robotCasilla.transform.GetChild(1).gameObject.SetActive(true);
                                }
                                else {
                                    robotCasilla.transform.GetChild(0).gameObject.SetActive(true);
                                }
                            }
                        }
                    }
                    else {
                        prints[4] = false;
                    }
                }

                if (numFila - j >= 0 && numCol - i - 1 >= 0) {
                    robotCasilla = casillas[numFila - j][numCol - i - 1];
                    if (checkPrint(robotCasilla) && prints[5] || prints[5] && attack && robotCasilla.tag == "factory" || prints[0] && attack && robotCasilla.tag == "ocupada") {
                        if (j >= distance - 1) {
                            robotCasilla.tag = nameCasilla;
                            if (pintar) {
                                robotCasilla.GetComponent<SpriteRenderer>().color = color;
                                if (attack){
                                    robotCasilla.transform.GetChild(1).gameObject.SetActive(true);
                                }
                                else {
                                    robotCasilla.transform.GetChild(0).gameObject.SetActive(true);
                                }
                            }
                        }
                    }
                    else {
                        prints[5] = false;
                    }
                }

                if (numFila - j >= 0 && numCol + i + 1 < casillas[numFila - j].Count) {
                    robotCasilla = casillas[numFila - j][numCol + i + 1];
                    if (checkPrint(robotCasilla) && prints[6] || prints[6] && attack && robotCasilla.tag == "factory" || prints[0] && attack && robotCasilla.tag == "ocupada") {
                        if (j >= distance - 1) {
                            robotCasilla.tag = nameCasilla;
                            if (pintar) {
                                robotCasilla.GetComponent<SpriteRenderer>().color = color;
                                if (attack) {
                                    robotCasilla.transform.GetChild(1).gameObject.SetActive(true);
                                }
                                else {
                                    robotCasilla.transform.GetChild(0).gameObject.SetActive(true);
                                }
                            }
                        }
                    }
                    else {
                        prints[6] = false;
                    }
                }

                if (numFila + j < casillas.Count && numCol - i - 1 >= 0) {
                    robotCasilla = casillas[numFila + j][numCol - i - 1];
                    if (checkPrint(robotCasilla) && prints[7] || prints[7] && attack && robotCasilla.tag == "factory" || prints[0] && attack && robotCasilla.tag == "ocupada") {
                        if (j >= distance - 1) {
                            robotCasilla.tag = nameCasilla;
                            if (pintar) {
                                robotCasilla.GetComponent<SpriteRenderer>().color = color;
                                if (attack) {
                                    robotCasilla.transform.GetChild(1).gameObject.SetActive(true);
                                }
                                else {
                                    robotCasilla.transform.GetChild(0).gameObject.SetActive(true);
                                }
                            }
                        }
                    }
                    else {
                        prints[7] = false;
                    }
                }
            }
            if (distance -1 > 0) {
                distance--;
            }
            printCas--;
        }
    }

    bool checkPrint(GameObject casilla) {
        bool printNow = true;
        for (int i = 0; i < objectsBlocked.Count; i++) {
            if(casilla.tag == objectsBlocked[i] || casilla.tag == "ocupada") {
                printNow = false;
            }
        }
        return printNow;
    }

    public bool checkFabrica() {
        print(numAttack, true, 1, Color.red, true, false);
        bool attack = false;
        for(int i = 0; i < GameManager.GetComponent<GameManager>().FactoriesA.Count; i++) {
            FactoryManager FM = GameManager.GetComponent<GameManager>().FactoriesA[i].GetComponent<FactoryManager>();
            for (int j = 0; j < FM.positions.Count; j++) {
                if (FM.positions[j].tag == "attack") {
                    attack = true;
                }
            }
            if (attack) {
                GameManager.GetComponent<GameManager>().robotSelect = this.gameObject;
                GameManager.GetComponent<GameManager>().attackFactory(GameManager.GetComponent<GameManager>().FactoriesA[i]);
                break;
            }
        }
        return attack;
    }

    
    /*Se llama para pintar numCasillas en cualquier dirección. El primer parametro es si se aumenta en x (+1) se disminuye
    en x(-1) o no se mueve en x (0). El segundo lo mismo en y.*/
    void whatPrint(int directionX, int directionY, bool print, Color color, bool attack, int numPintar) {
        block = false;
        for (int i = 1; i < (numPintar + 1); i++) {
            if((numFila + (i * directionX)) >= 0 && (numFila + (i * directionX)) < casillas[0].Count
                && (numCol + (i * directionY)) >= 0 && (numCol + (i * directionY)) < casillas.Count) {
                robotCasilla = casillas[numFila + (i * directionX)][numCol + (i * directionY)];
                if (!attack) {
                    if (robotCasilla.tag != "block" && !block && robotCasilla.tag != "ocupada") {
                        if (print) {
                            robotCasilla.GetComponent<SpriteRenderer>().color = color;
                        }
                        robotCasilla.tag = "move";
                    }
                    else {
                        block = true;
                    }
                }

                else {
                    if (robotCasilla.tag != "block" && !block) {
                        if(robotCasilla.tag == "ocupada") {
                            if (returnTagRobotCasilla(numFila + (i * directionX), numCol + (i * directionY))
                                    == GameManager.GetComponent<GameManager>().robotTurn) {
                                block = true;
                            }
                            else {
                                if (print) {
                                    robotCasilla.GetComponent<SpriteRenderer>().color = color;
                                }
                                robotCasilla.tag = "attack";
                            }
                        }
                        else {
                            if (print) {
                                robotCasilla.GetComponent<SpriteRenderer>().color = color;
                            }
                            robotCasilla.tag = "attack";
                        }
                    }
                    else {
                        block = true;
                    }
                }
            }
        }
    }

    void whatPrintOther(int firstDirectionX, int firstDirectionY, bool print, Color color, bool attack, int numPintar) {
        block = false;
        if ((numFila + (1 * firstDirectionX)) >= 0 && (numFila + (1 * firstDirectionX)) < casillas[0].Count
                && (numCol + (1 * firstDirectionY)) >= 0 && (numCol + (1 * firstDirectionY)) < casillas.Count) {

            robotCasilla = casillas[numFila + (1 * firstDirectionX)][numCol + (1 * firstDirectionY)];
            newNumFila = numFila + (1 * firstDirectionX);
            newNumCol = numCol + (1 * firstDirectionY);
            Debug.Log("La casilla es: " + newNumFila + " " + newNumCol);
            if (!attack) { 
                if (robotCasilla.tag != "block" && !block && robotCasilla.tag != "ocupada") {
                    if (print) {
                        robotCasilla.GetComponent<SpriteRenderer>().color = color;
                    }
                    robotCasilla.tag = "move";

                }
                else {
                    block = true;
                }
            }
            else {
                if (robotCasilla.tag != "block" && !block) {
                    if (print) {
                        robotCasilla.GetComponent<SpriteRenderer>().color = color;
                    }
                    robotCasilla.tag = "attack";
                }
                else {
                    block = true;
                }
            }
        }

        if (!block) {
            blockX = false;
            blockY = false;
            for (int i = 1; i < (numPintar - 1); i++) {
                if ((newNumFila + (i * firstDirectionX)) >= 0 && (newNumFila + (i * firstDirectionX)) < casillas[0].Count) {
                    robotCasilla = casillas[newNumFila + (i * firstDirectionX)][newNumCol];
                    if (!attack) { 
                        if (robotCasilla.tag != "block" && !blockX && robotCasilla.tag != "ocupada") {
                            if (print) {
                                robotCasilla.GetComponent<SpriteRenderer>().color = color;
                            }
                            robotCasilla.tag = "move";
                        }
                        else {
                            blockX = true;
                        }
                    }

                    else {
                        if (robotCasilla.tag != "block" && !blockX) {
                            if(robotCasilla.tag == "ocupada") {
                               if(returnTagRobotCasilla(newNumFila + (i * firstDirectionX), newNumCol) 
                                    == GameManager.GetComponent<GameManager>().robotTurn) {
                                    blockX = true;
                                }
                                else {
                                    if (print) {
                                        robotCasilla.GetComponent<SpriteRenderer>().color = color;
                                    }
                                    robotCasilla.tag = "attack";
                                }
                            }
                            else {
                                if (print) {
                                    robotCasilla.GetComponent<SpriteRenderer>().color = color;
                                }
                                robotCasilla.tag = "attack";
                            }
                        }
                        else {
                            blockX = true;
                        }
                    }
                }
                if ((newNumCol + (i * firstDirectionY)) >= 0 && (newNumCol + (i * firstDirectionY)) < casillas.Count) {
                    robotCasilla = casillas[newNumFila][newNumCol + (i * firstDirectionY)];
                    if (!attack) {
                        if (robotCasilla.tag != "block" && !blockY && robotCasilla.tag != "ocupada") {
                            if (print) {
                                robotCasilla.GetComponent<SpriteRenderer>().color = color;
                            }
                            robotCasilla.tag = "move";
                        }
                        else {
                            blockY = true;
                        }
                    }
                    else {
                        if (robotCasilla.tag != "block" && !blockY) {
                            if (robotCasilla.tag == "ocupada") {
                                if (returnTagRobotCasilla(newNumFila, newNumCol + (i * firstDirectionY))
                                     == GameManager.GetComponent<GameManager>().robotTurn) {
                                    blockX = true;
                                }
                                else {
                                    if (print) {
                                        robotCasilla.GetComponent<SpriteRenderer>().color = color;
                                    }
                                    robotCasilla.tag = "attack";
                                }
                            }

                            else {
                                if (print) {
                                    robotCasilla.GetComponent<SpriteRenderer>().color = color;
                                }
                                robotCasilla.tag = "attack";
                            }

                        }
                        else {
                            blockY = true;
                        }
                    }
                    
                }
            }
        }
    }

    string returnTagRobotCasilla(int x, int y) {
        string tag = "";
        //Comprobamos robots a
        for (int i = 0; i < GameObject.FindGameObjectsWithTag("robotA").Length; i++) {
            if(GameObject.FindGameObjectsWithTag("robotA")[i].GetComponent<dondeMover>().numFila == x
               && GameObject.FindGameObjectsWithTag("robotA")[i].GetComponent<dondeMover>().numCol == y)
            {
                Debug.Log("Robot encontrado");
                tag = GameObject.FindGameObjectsWithTag("robotA")[i].tag;
            }
        }

        for (int i = 0; i < GameObject.FindGameObjectsWithTag("robotB").Length; i++) {
            if (GameObject.FindGameObjectsWithTag("robotB")[i].GetComponent<dondeMover>().numFila == x
               && GameObject.FindGameObjectsWithTag("robotB")[i].GetComponent<dondeMover>().numCol == y)
            {
                Debug.Log("Robot encontrado");
                tag = GameObject.FindGameObjectsWithTag("robotB")[i].tag;
            }
        }

        return tag;
    }
}


