﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TypeRobot { NORMAL, TANK, AERODYNAMIC, OVNI, NULL}
public enum RobotZone { HEAD, TORSO, LEGS, NULL}
public enum RobotWeapons { KNIFE, SWORD, GUN, SHOTGUN, MACHINEGUN, SNIPPERGUN, LASERWEAPON, NULL}
public enum AnimationType { IDLE, ATTACK, NULL};
public enum RobotCreated { ROBOT1, ROBOT2, ROBOT3, ROBOT4, ROBOTINFECTED}

public class RobotParts : MonoBehaviour {
    
    public List<List<List<GameObject>>> RobotPrefabs;
    public List<List<string>> animationsArms;
    public List<List<string>> animationsWeapons;
    public List<List<string>> objectsBlocked;
    public List<List<GameObject>> UISprites;
    //public List<List<GameObject>> RobotArms;

    [Header("Poner prefabs. 3 posiciones: cabeza, torso y por último piernas. 5 List por tipo")]
    public List<GameObject> Normal1Prefabs;
    public List<GameObject> Normal2Prefabs;
    public List<GameObject> Normal3Prefabs;
    public List<GameObject> Normal4Prefabs;
    public List<GameObject> NormalInfectedPrefabs;
    public List<GameObject> Tank1Prefabs;
    public List<GameObject> Tank2Prefabs;
    public List<GameObject> Tank3Prefabs;
    public List<GameObject> Tank4Prefabs;
    public List<GameObject> TankInfectedPrefabs;
    public List<GameObject> Aero1Prefabs;
    public List<GameObject> Aero2Prefabs;
    public List<GameObject> Aero3Prefabs;
    public List<GameObject> Aero4Prefabs;
    public List<GameObject> AeroInfectedPrefabs;
    public List<GameObject> Ovni1Prefabs;
    public List<GameObject> Ovni2Prefabs;
    public List<GameObject> Ovni3Prefabs;
    public List<GameObject> Ovni4Prefabs;
    public List<GameObject> OvniInfectedPrefabs;
    

    [Header("Animations de armas y torsos")]
    public List<string> animationWeaponsAttack;
    public List<string> animationWeaponsIdle;
    public List<string> animationArmsAttack;
    public List<string> animationArmsIdle;

    [Header("Tags de los objetos que bloquean segun el tipo de robot.")]
    public List<string> normalObjectsBlock;
    public List<string> tankObjectsBlock;
    public List<string> aeroObjectsBlock;
    public List<string> ovniObjectsBlock;

    [Header("Sprites de las partes de los robots para el UI")]
    public List<GameObject> normalUI;
    public List<GameObject> tankUI;
    public List<GameObject> aeroUI;
    public List<GameObject> ovniUI;

    [Header("Animaciones de explosiones")]
    public Sprite test;
    public List<GameObject> explosionAnimations;

    private List<List<GameObject>> tmp;

    /*[Header("Order: KNIFE, ENERGYSWORD, MACHINEGUN, GUN, SHOOTGUN, \n")]
    [Header("SNIPPERGUN, LASERWEAPON. Dejar en blanco si no hay.")]*/
    /*public List<GameObject> NormalArms;
    public List<GameObject> TankArms;
    public List<GameObject> AeroArms;
    public List<GameObject> OvniArms;*/

    private void Awake() {
        objectsBlocked = new List<List<string>>();
        RobotPrefabs = new List<List<List<GameObject>>>();
        tmp = new List<List<GameObject>>();
        RobotPrefabs.Add(new List<List<GameObject>>());
        RobotPrefabs.Add(new List<List<GameObject>>());
        RobotPrefabs.Add(new List<List<GameObject>>());
        RobotPrefabs.Add(new List<List<GameObject>>());
        animationsArms = new List<List<string>>();
        animationsWeapons = new List<List<string>>();
        UISprites = new List<List<GameObject>>();
        //RobotArms = new List<List<GameObject>>();
        /*for(int i = 0; i < 4; i++) {
            RobotPrefabs[i] = new List<GameObject>();
        }*/
        /*RobotPrefabs.Add(NormalPrefabs);
        RobotPrefabs.Add(TankPrefabs);
        RobotPrefabs.Add(AeroPrefabs);
        RobotPrefabs.Add(OvniPrefabs);*/

        animationsArms.Add(animationArmsIdle);
        animationsArms.Add(animationArmsAttack);
        animationsWeapons.Add(animationWeaponsIdle);
        animationsWeapons.Add(animationWeaponsAttack);

        objectsBlocked.Add(normalObjectsBlock);
        objectsBlocked.Add(tankObjectsBlock);
        objectsBlocked.Add(aeroObjectsBlock);
        objectsBlocked.Add(ovniObjectsBlock);

        UISprites.Add(normalUI);
        UISprites.Add(tankUI);
        UISprites.Add(aeroUI);
        UISprites.Add(ovniUI);

        /*public List<GameObject> Normal1Prefabs;
    public List<GameObject> Normal2Prefabs;
    public List<GameObject> Normal3Prefabs;
    public List<GameObject> Normal4Prefabs;
    public List<GameObject> NormalInfectedPrefabs;*/

        RobotPrefabs[0].Add(Normal1Prefabs);
        RobotPrefabs[0].Add(Normal2Prefabs);
        RobotPrefabs[0].Add(Normal3Prefabs);
        RobotPrefabs[0].Add(Normal4Prefabs);
        RobotPrefabs[0].Add(NormalInfectedPrefabs);

        RobotPrefabs[1].Add(Tank1Prefabs);
        RobotPrefabs[1].Add(Tank2Prefabs);
        RobotPrefabs[1].Add(Tank3Prefabs);
        RobotPrefabs[1].Add(Tank4Prefabs);
        RobotPrefabs[1].Add(TankInfectedPrefabs);

        RobotPrefabs[2].Add(Aero1Prefabs);
        RobotPrefabs[2].Add(Aero2Prefabs);
        RobotPrefabs[2].Add(Aero3Prefabs);
        RobotPrefabs[2].Add(Aero4Prefabs);
        RobotPrefabs[2].Add(AeroInfectedPrefabs);

        RobotPrefabs[3].Add(Ovni1Prefabs);
        RobotPrefabs[3].Add(Ovni2Prefabs);
        RobotPrefabs[3].Add(Ovni3Prefabs);
        RobotPrefabs[3].Add(Ovni4Prefabs);
        RobotPrefabs[3].Add(OvniInfectedPrefabs);
    }
}
