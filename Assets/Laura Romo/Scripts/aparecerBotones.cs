﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aparecerBotones : MonoBehaviour {

    [SerializeField] GameObject Botones;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void showButtons() {
        Botones.SetActive(true);
        GetComponent<Animator>().enabled = false;
        GetComponent<BoxCollider2D>().enabled = false;
    }

    private void OnMouseDown() {
        GetComponent<Animator>().enabled = true;
    }
}
