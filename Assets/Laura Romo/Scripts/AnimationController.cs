﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{

    public string armsAnim;
    public string weaponsAnim;
    public string walkAnim;
    public bool changeAnim;
    public bool attack;
    public Robot robot;

    // Start is called before the first frame update
    void Start() {
        attack = false;
        changeAnim = false;
    }

    // Update is called once per frame
    void Update() {
        if(transform.parent != null) {
            if (transform.parent.GetComponent<AnimationController>() != null) {
                armsAnim = transform.parent.GetComponent<AnimationController>().armsAnim;
                weaponsAnim = transform.parent.GetComponent<AnimationController>().weaponsAnim;
            }
        }
    }
}
