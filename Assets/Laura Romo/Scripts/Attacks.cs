﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attacks {
    int powerAttack;
    int distanceAttack;
    bool diagonal;
    string name;

    public Attacks(int _powerAttack, int _distanceAttack, bool _diagonal, string _name) {
        powerAttack = _powerAttack;
        distanceAttack = _distanceAttack;
        diagonal = _diagonal;
        name = _name;
    }

    public int getPowerAttack() {
        return powerAttack;
    }

    public void setPowerAttack(int _powerAttack) {
        powerAttack = _powerAttack;
    }

    public int getDistanceAttack() {
        return distanceAttack;
    }

    public void setDistanceAttack(int _distanceAttack) {
        distanceAttack = _distanceAttack;
    }

    public bool getDiagonal() {
        return diagonal;
    }

    public void setDiagonal(bool _diagonal) {
        diagonal = _diagonal;
    }
}