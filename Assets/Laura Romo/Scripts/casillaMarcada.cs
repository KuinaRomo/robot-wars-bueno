﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class casillaMarcada : MonoBehaviour {

   

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnMouseEnter() {
        this.gameObject.GetComponent<SpriteRenderer>().color = Color.black;
        Color tmp = this.gameObject.GetComponent<SpriteRenderer>().color;
        tmp.a = 0.5f;
        this.gameObject.GetComponent<SpriteRenderer>().color = tmp;
    }

    private void OnMouseExit(){
        this.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        Color tmp = gameObject.GetComponent<SpriteRenderer>().color;
        tmp.a = 1;
        this.gameObject.GetComponent<SpriteRenderer>().color = tmp;
    }
}
