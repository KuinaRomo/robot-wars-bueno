﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ControlMenuHand : MonoBehaviour {

    public Texture2D cursorImage;
    
    // Use this for initialization
    void Start () {
        //GetComponent<RectTransform>().position.x = 
        Cursor.SetCursor(cursorImage, Vector2.zero, CursorMode.Auto);
    }
	
	// Update is called once per frame
	void Update () {
        GetComponent<RectTransform>().position = new Vector3(Input.mousePosition.x, 
                                                            Input.mousePosition.y,
                                                            GetComponent<RectTransform>().position.z);
	}


}
