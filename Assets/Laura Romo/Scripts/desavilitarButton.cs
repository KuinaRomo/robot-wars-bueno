﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class desavilitarButton : MonoBehaviour {

    [SerializeField] GameObject buttonPasarTurno;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(GetComponent<GameManager>().robotTurn != GetComponent<GameManager>().robotsControled) {
            buttonPasarTurno.SetActive(false);
        }
        else {
            buttonPasarTurno.SetActive(true);
        }
	}
}
