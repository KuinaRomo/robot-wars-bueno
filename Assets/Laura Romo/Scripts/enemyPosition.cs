﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyPosition : MonoBehaviour {
    private int coste;
    private int costeTemp;
    private int costeFabrica;
    private int totalRobots;
    private GameObject GameManager;
    private GameObject robotObjetivo;
    private GameObject fabricaObjetivo;
    private GameObject actualGameObject;
    public List<GameObject> path;
    private int totalMover;
    private int totalFabricas;
    private bool moving;
    private bool calculated;
    private bool inPlaceX;
    private bool inPlaceY;
    private GameObject enemyRobot;
    private float moveX;
    private float moveY;
    private float contadorMoving;
    private int times;
    private int contadorDebug;
    [SerializeField] GameObject tablero;
    [SerializeField] dondeMover DondeMover;
    List<Robot> temporal = new List<Robot>();
    ResourcesManager RM;
    private int contadorTemp;

    //Diferentes comportaminetos
    public bool objetivoFabrica;
    public bool objetivoRobots;

    // Use this for initialization
    void Start () {
        objetivoRobots = true;
        moveX = 0;
        moveY = 0;
        contadorTemp = 0;
        contadorMoving = 0;
        times = 0;
        calculated = false;
        inPlaceX = false;
        inPlaceY = false;
        DondeMover = this.GetComponent<dondeMover>();
        GameManager = GameObject.FindGameObjectWithTag("GameManager");
        path = new List<GameObject>();
        coste = -1;
        totalMover = 0;
        calculated = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (moving) {
            moveRobot(actualGameObject, this.gameObject);
        }
    }

    public void moveRobot(GameObject casilla, GameObject robotSelected) {
        if (casilla.tag == "move") {
            if (!calculated) {
                moveX = (casilla.transform.position.x - robotSelected.transform.position.x) / 10;
                moveY = (casilla.transform.position.y - robotSelected.transform.position.y) / 10;
                calculated = true;
            }
            if (!inPlaceX) {
                if (contadorMoving > 0.02f) {
                    if (times < 10) {
                        robotSelected.transform.position = new Vector3(robotSelected.transform.position.x + moveX, robotSelected.transform.position.y, robotSelected.transform.position.z);
                        times++;
                        contadorMoving = 0;
                    }
                    else {
                        inPlaceX = true;
                        times = 0;
                    }
                }

                else {
                    contadorMoving += Time.deltaTime;
                }

            }
            else if (!inPlaceY) {
                if (contadorMoving > 0.02f) {
                    if (times < 10) {
                        robotSelected.transform.position = new Vector3(robotSelected.transform.position.x, robotSelected.transform.position.y + moveY, robotSelected.transform.position.z);
                        times++;
                        contadorMoving = 0;
                    }
                    else {
                        inPlaceY = true;
                    }
                }

                else {
                    contadorMoving += Time.deltaTime;
                }
            }
            else {
                GameManager.GetComponent<GameManager>().cleanCasillas();
                casilla.tag = "ocupada";
                moving = false;
                calculated = false;
                inPlaceX = false;
                inPlaceY = false;
                times = 0;
                atacadoAlEnemigo();
            }
        }
    }

    public void newIA() {
        Debug.Log(this.name);
        if (objetivoFabrica) {
            moveOrAttackFactory();
        }
        else if (objetivoRobots) {
            //Busco robot mas debil
            if(GameManager.GetComponent<GameManager>().robotsA.Count > 0) {
                float actualLife = -1;
                robotObjetivo = null;
                for(int i = 0; i < GameManager.GetComponent<GameManager>().robotsA.Count; i++) {
                    if(GameManager.GetComponent<GameManager>().robotsA[i] != null) {
                        if(GameManager.GetComponent<GameManager>().robotsA[i].getRobot() != null) {
                            dondeMover temp = GameManager.GetComponent<GameManager>().robotsA[i].getRobot().GetComponent<dondeMover>();
                            if (temp.life < actualLife || actualLife == -1) {
                                robotObjetivo = GameManager.GetComponent<GameManager>().robotsA[i].getRobot();
                                actualLife = temp.life;
                            }
                        }
                    }
                }
                if (robotObjetivo != null) {
                    moveOrAttackRobot(robotObjetivo);
                }
                else {
                    moveOrAttackFactory();
                }
            }
            else {
                moveOrAttackFactory();
            }
        }
    }

    private void moveOrAttackRobot(GameObject robot) {
        path.Clear();
        if (robotObjetivo != null) {
            GameManager.GetComponent<aStar>().CalcularDistancia(robot, this.gameObject, true, true, true, false, false);
        }
        GameManager.GetComponent<GameManager>().recalculateOcupadas();
        this.GetComponent<dondeMover>().printMove(true, false);
        GameManager.GetComponent<GameManager>().recalculateOcupadas();
        //Debug.Log("Tamaño path: " + path.Count);
        for(int i = 0; i < path.Count; i++) {
            for(int j = 0; j < GameManager.GetComponent<GameManager>().robotsB.Count; j++) {
                if (GameManager.GetComponent<GameManager>().casillas[GameManager.GetComponent<GameManager>().robotsB[j].getRobot().GetComponent<dondeMover>().numFila]
                    [GameManager.GetComponent<GameManager>().robotsB[j].getRobot().GetComponent<dondeMover>().numCol] == path[i]) {
                    Debug.Log("Esta ocupda y la elimino");
                    path[i] = null;
                }
            }
            
        }

        for (int i = 0; i < path.Count; i++) {
            if(path[i] != null) {
                if (path[i].tag == "move") {
                    totalMover++;
                    actualGameObject = path[i];
                    break;
                }
            }
            
        }

        if (totalMover > 0) {
            //Check if esta mas cerca de fabricas
            GameManager.GetComponent<GameManager>().casillaObjetivo = actualGameObject;
            GameManager.GetComponent<GameManager>().robotSelect = gameObject;
            GameManager.GetComponent<GameManager>().moving = true;
            //this.GetComponent<dondeMover>().movido = true;
            totalMover = 0;
            /*moving = true;
            GameManager.GetComponent<GameManager>().recalculatedCasilla(actualGameObject, this.gameObject);
            this.GetComponent<dondeMover>().movido = true;
            totalMover = 0;*/
        }

        else {
            this.GetComponent<dondeMover>().movido = true;
            atacadoAlEnemigo();
        }
    }

    private void moveOrAttackFactory() {
        if (!DondeMover.checkFabrica()) {
            totalFabricas = GameManager.GetComponent<GameManager>().FactoriesA.Count;
            coste = -1;
            path.Clear();
            GameManager.GetComponent<GameManager>().recalculateOcupadas();
            moveToFabrica();
        }
        else {
            GetComponent<dondeMover>().atacado = true;
            if (!GameManager.GetComponent<GameManager>().checkAtacado()) {
                GameManager.GetComponent<GameManager>().totalMovido++;
                GameManager.GetComponent<GameManager>().tirarIA = true;
            }
            else {
                //Debug.Log("CHECK ATACANDO ES FALSE");
            }
        }


    }

    public void pruebas() {
        DondeMover.checkFabrica();
        /*If puedo atacarla miro si puedo atacar la ataco.*/
        /*Si no me muevo a atacar un robot siempre que eso me acerque mas a la fabrica.*/
    }

    public void turnIA() {
        /*Mirar si puedes atacar a la fabrica*/
        if (!DondeMover.checkFabrica()) {
            actualGameObject = null;
            contadorTemp++;
            coste = -1;
            /*Primero uso el algoritmo a star para ver cual es el robot mas cercano a mi*/
            totalRobots = GameManager.GetComponent<GameManager>().robotsA.Count;
            for(int i = 0; i < totalRobots; i++) {
                if(GameManager.GetComponent<GameManager>().robotsA[i] != null) {
                    temporal = GameManager.GetComponent<GameManager>().robotsA;
                    if (GameManager.GetComponent<GameManager>().robotsA[i].getRobot() != null) {
                        actualGameObject = GameManager.GetComponent<GameManager>().robotsA[i].getRobot();
                        costeTemp = GameManager.GetComponent<aStar>().CalcularDistancia(actualGameObject, this.gameObject, false, true, true, false, false);
                        if(coste == -1) {
                            robotObjetivo = actualGameObject;
                            coste = costeTemp;
                        }
                        else if (costeTemp < coste){
                            coste = costeTemp;
                            robotObjetivo = actualGameObject;
                        }
                        else {
                            Debug.Log("Entro aqui y no se si deberia. " + coste);
                            //robotObjetivo = null;
                        }
                    }
                }
            }
            Debug.Log("El robot mas cercano es: " + robotObjetivo);
            /*Chequeo la distancia hasta la fabrica mas cercana*/
            coste = -1;
            totalFabricas = GameManager.GetComponent<GameManager>().FactoriesA.Count;
            for (int i = 0; i < totalFabricas; i++) {
                if (GameManager.GetComponent<GameManager>().FactoriesA[i] != null) {
                    actualGameObject = GameManager.GetComponent<GameManager>().FactoriesA[i];
                    costeTemp = GameManager.GetComponent<aStar>().CalcularDistancia(actualGameObject, this.gameObject, false, false, true, false, false);
                    //costeTemp = GameManager.GetComponent<aStar>().CalcularDistancia(actualGameObject, this.gameObject, false);
                    if (coste == -1) {
                        costeFabrica = costeTemp;
                        fabricaObjetivo = actualGameObject;
                    }
                    else if (costeTemp < coste) {
                        costeFabrica = costeTemp;
                        fabricaObjetivo = actualGameObject;
                    }
                }
            }
            Debug.Log("Distancia a la fabrica mas cercana: " + costeFabrica);
            /*Si encuentro algun robot paso de nuevo el a* para recoger la ruta hasta él mas cercano
             y busco que cual es el punto más cercano de la ruta al que me puedo mover*/
            if (totalRobots > 0) {
                contadorDebug++;
                path.Clear();
                if(robotObjetivo != null) {
                    GameManager.GetComponent<aStar>().CalcularDistancia(robotObjetivo, this.gameObject, true, true, true, false, false);
                }
                this.GetComponent<dondeMover>().printMove(true, false);
                //Debug.Log("Tamaño path: " + path.Count);
                for (int i = 0; i < path.Count; i++) {
                    if(path[i].tag == "move") {
                        totalMover++;
                        actualGameObject = path[i];
                        break;   
                    }
                }
                int tempFabrica = GameManager.GetComponent<aStar>().CalcularDistancia(actualGameObject, fabricaObjetivo, true, true, false, true, false);
                if (totalMover > 0) {
                    //Check if esta mas cerca de fabricas
                    moving = true;
                    GameManager.GetComponent<GameManager>().recalculatedCasilla(actualGameObject, this.gameObject);
                    this.GetComponent<dondeMover>().movido = true;
                    totalMover = 0;
                }

                else {
                    this.GetComponent<dondeMover>().movido = true;
                    atacadoAlEnemigo();
                }
                Debug.Log("Me muevo al robot mas cercano: " + costeFabrica);
            }
        
            else {
                //Aqui ir a por factories. De momento pasa turno
                Debug.Log("Moverse hacia la fabrica");
                GameManager.GetComponent<GameManager>().recalculateOcupadas();
                moveToFabrica();
                //GameManager.GetComponent<GameManager>().pasarTurno();
            }

            /*Ahora compruevo si puedo atacar a ese objetivo y si puedo le ataco.*/
        }
        else {
            GetComponent<dondeMover>().atacado = true;
            if (!GameManager.GetComponent<GameManager>().checkAtacado()) {
                GameManager.GetComponent<GameManager>().totalMovido++;
                GameManager.GetComponent<GameManager>().tirarIA = true;
            }
            else {
                Debug.Log("CHECK ATACANDO ES FALSE");
            }
        }
    }

    private void moveToFabrica() {
        for (int i = 0; i < totalFabricas; i++) {
            if (GameManager.GetComponent<GameManager>().FactoriesA[i] != null) {
                GameManager.GetComponent<GameManager>().recalculateOcupadas();
                actualGameObject = GameManager.GetComponent<GameManager>().FactoriesA[i];
                costeTemp = GameManager.GetComponent<aStar>().CalcularDistancia(actualGameObject, this.gameObject, true, false, true, false, false);
                //Debug.Log("La distancia hasta la fabrica es de: " + costeTemp);
                if (coste == -1) {
                    costeFabrica = costeTemp;
                    fabricaObjetivo = actualGameObject;
                }
                else if (costeTemp < coste) {
                    costeFabrica = costeTemp;
                    fabricaObjetivo = actualGameObject;
                }
            }
        }
        //GameManager.GetComponent<GameManager>().recalculateOcupadas();
        this.GetComponent<dondeMover>().printMove(true, false);

        for (int i = 0; i < path.Count; i++) {
            //Debug.Log("NO ENCUNETRA CASILLA TAGEADA");
            if (path[i].tag == "move") {
                
                totalMover++;
                actualGameObject = path[i];
                break;
            }
        }

        if (totalMover > 0) {
            //Check if esta mas cerca de fabricas
            //Debug.Log("Muevo");
            //moving = true;
            //GameManager.GetComponent<GameManager>().recalculatedCasilla(actualGameObject, this.gameObject);
            GameManager.GetComponent<GameManager>().casillaObjetivo = actualGameObject;
            GameManager.GetComponent<GameManager>().robotSelect = gameObject;
            GameManager.GetComponent<GameManager>().moving = true;
            //this.GetComponent<dondeMover>().movido = true;
            totalMover = 0;
        }
        
    }

    public void atacadoAlEnemigo() {
        this.GetComponent<dondeMover>().printMove(true, true);
        GameManager.GetComponent<GameManager>().atacando = true;
        GameManager.GetComponent<GameManager>().robotSelect = gameObject;
        GameManager.GetComponent<GameManager>().atacar(robotObjetivo, 
            GameManager.GetComponent<GameManager>().returnRobot(gameObject).weapon);
        GameManager.GetComponent<GameManager>().atacando = false;
        GetComponent<dondeMover>().atacado = true;
        if (!GameManager.GetComponent<GameManager>().checkAtacado()) {
            GameManager.GetComponent<GameManager>().totalMovido++;
            GameManager.GetComponent<GameManager>().tirarIA = true;
        }
        else {
            //Debug.Log("CHECK ATACANDO ES FALSE");
        }
    }
}
