﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node {

    int x;
    int y;
    int cost; //Total de pasos dados = g
    int distanceManhattan; //Distancia Manhattan o calculo de la distancia entre el punto de origen y el punto de destino = h
    int totalCost; //h + g = f
    int father; //Posicion en la closeList del node padre

  	public void setX(int _x) {
        x = _x;
        return;
    }

    public int getX() {
        return x;
    }

    public void setY(int _y) {
        y = _y;
        return;
    }

    public int getY() {
        return y;
    }

    public void setCost(int _cost) {
        cost = _cost;
        return;
    }

    public int getCost() {
        return cost;
    }

    public void setManhattan(int _distanceManhattan) {
        distanceManhattan = _distanceManhattan;
        return;
    }

    public int getManhattan() {
        return distanceManhattan;
    }

    public void setTotalCost(int _totalCost) {
        totalCost = _totalCost;
        return;
    }

    public int getTotalCost() {
        return totalCost;
    }

    public void setFather(int _father) {
        father = _father;
        return;
    }

    public int getFather() {
        return father;
    }
}
