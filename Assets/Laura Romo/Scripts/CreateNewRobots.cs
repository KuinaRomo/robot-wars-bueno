﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateNewRobots : MonoBehaviour {
    public List<GameObject> spawnBox;
    [SerializeField] GameManager GameManager;
    [SerializeField] GameObject Canvas;
    private GameObject objectiveBox;
    public ResourcesManager RM;
    [SerializeField]  List<Button> ButtonRobots;
    [SerializeField] List<GameObject> FactoriesA;
    [SerializeField] List<GameObject> FactoriesB;
    [SerializeField] CameraMovement cameraMovement;
    bool aparecido;

    public GameObject selectBox() {
        for(int i = 0; i < spawnBox.Count; i++)  {
            if(spawnBox[i].tag != "ocupada") {
                return spawnBox[i];
            }
        }
        return null;
    }

    // Use this for initialization
    void Start () {
        aparecido = false;
        if(Canvas != null) {
            Canvas.SetActive(false);
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnMouseDown() {
        if (checkPosibleMakeRobots() && !aparecido) {
            if(this.tag == "miniFactoryA" && GameManager.robotTurn == "robotA") {
                GameManager.selectedFactory = this.gameObject;
                Canvas.SetActive(true);
                cameraMovement.moveCamera = false;
                cameraMovement.changeSprite();
                aparecido = true;
            }
            else if(this.tag == "miniFactoryB" && GameManager.robotTurn == "robotB") {
                GameManager.selectedFactory = this.gameObject;
                Canvas.SetActive(true);
                cameraMovement.moveCamera = false;
                cameraMovement.changeSprite();
                aparecido = true;
            }
        }
        else if (aparecido) {
            aparecido = false;
            Canvas.SetActive(false);
            cameraMovement.moveCamera = true;
            cameraMovement.changeSprite();
        }
    }

    public void desactivarSelectRobotHud()  {
        aparecido = false;
        Canvas.SetActive(false);
        cameraMovement.moveCamera = true;
        cameraMovement.changeSprite();
    }

    private bool checkPosibleMakeRobots() {
        bool isPosible = false;
        if(GameManager.robotTurn == "robotA") {
            for(int i = 0; i < ButtonRobots.Count; i++) {
                for(int j = 0; j < FactoriesA.Count; j++) {
                    if (FactoriesA[j] != null) {
                        if (GameManager.robotsAPlantilla[i].getResources() >
                        FactoriesA[j].GetComponent<FactoryManager>().actualResources) {
                            ButtonRobots[i].interactable = false;
                        }
                        else {
                            ButtonRobots[i].interactable = true;
                            isPosible = true;
                        }
                    }
                }
                
            }
        }

        else {
            for (int i = 0; i < ButtonRobots.Count; i++) {

                for (int j = 0; j < FactoriesB.Count; j++) {
                    if(FactoriesB[j] != null) {
                        if (GameManager.robotsBPlantilla[i].getResources() >
                        FactoriesB[j].GetComponent<FactoryManager>().actualResources) {
                            ButtonRobots[i].interactable = false;
                        }
                        else {
                            ButtonRobots[i].interactable = true;
                            isPosible = true;
                        }
                    }
                    
                }
            }
        }

        return isPosible;
    }
}


