﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Return : MonoBehaviour
{

    MusicControler musicControler;


    // Start is called before the first frame update
    void Start() {
        musicControler = GameObject.Find("MusicController").GetComponent<MusicControler>();
    }

    // Update is called once per frame
    void Update() {
        
    }

    public void returnMenu() {
        musicControler.audioSource[1].Stop();
        musicControler.audioSource[1].loop = false;
        SceneManager.LoadScene("MainMenu");
    }
}
