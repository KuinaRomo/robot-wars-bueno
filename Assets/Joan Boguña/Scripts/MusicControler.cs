﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicControler : MonoBehaviour {

    public MusicClass Music;
    [SerializeField] private UnityEngine.UI.Slider[] sliders;
    [SerializeField] private UnityEngine.UI.Text[] numVolume;
    public GameObject canvasOptions;
    private static MusicControler _instance;
    public AudioSource[] audioSource;
    public float backMusicVol;
    public float soundEffectsVol;
    private float temp = 0;

    [SerializeField] private GameManager gameManager;
    [SerializeField] private Canvas menuCanvas;
    GameObject PauseMenu;
    public GameObject music;

    public static MusicControler Instance {
        get {
            if (_instance == null) {
                _instance = GameObject.FindObjectOfType<MusicControler>();
            }

            return _instance;
        }
    }

    void Awake() {
        DontDestroyOnLoad(gameObject);
    }

    public void Start() {
        audioSource = GetComponents<AudioSource>();
        audioSource[0].clip = Music.audioClips[0];
        audioSource[0].Play();

        sliders[0].value = audioSource[0].volume;
        sliders[1].value = audioSource[1].volume;
        backMusicVol = audioSource[0].volume;
        soundEffectsVol = audioSource[1].volume;
        temp = backMusicVol * 100;
        numVolume[0].text = ((int)temp).ToString();
        temp = soundEffectsVol * 100;
        numVolume[1].text = ((int)temp).ToString();
    }

    public void Update() {
        /*if (Input.GetKeyDown(KeyCode.Space))
        {
            audioSource[1].clip = Music.audioClips[1];
            audioSource[1].Play();
        }*/
        try
        {
            sliders[0].onValueChanged.AddListener(delegate
            {
                audioSource[0].volume = sliders[0].value;
                backMusicVol = sliders[0].value;
                temp = backMusicVol * 100;
                numVolume[0].text = ((int)temp).ToString();
            });

            sliders[1].onValueChanged.AddListener(delegate
            {
                audioSource[1].volume = sliders[1].value;
                soundEffectsVol = sliders[1].value;
                temp = soundEffectsVol * 100;
                numVolume[1].text = ((int)temp).ToString();
            });
        }
        catch {
            Debug.Log("Missing sliders");
        }
    }

    public void ReturnMainMenu(Canvas canvas) {
        audioSource[1].clip = Music.audioClips[1];
        audioSource[1].Play();

        if (SceneManager.GetActiveScene().name != "LauraRomo") {
            canvas.gameObject.SetActive(false);
            menuCanvas.gameObject.SetActive(true);
        }
        else {
            if (PauseMenu == null) {
                gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
                PauseMenu = gameManager.pauseMenu;
            }
            PauseMenu.SetActive(true);
            canvas.gameObject.SetActive(false);
        }
    }

    public void playSoundEffects() {
        audioSource[1].clip = Music.audioClips[1];
        audioSource[1].Play();
    }
}
