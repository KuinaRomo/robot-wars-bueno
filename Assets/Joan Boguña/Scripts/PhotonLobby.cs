﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotonLobby : MonoBehaviourPunCallbacks {

    public static PhotonLobby lobby;

    public GameObject BattleButton;
    public GameObject CancelButton;
   
    

    private void Awake()
    {
        lobby = this;
        
    }

    // Use this for initialization
    void Start () {
        PhotonNetwork.ConnectUsingSettings();
	}

    public override void OnConnectedToMaster()
    {
        Debug.Log("Player has connected to server");
       
        PhotonNetwork.AutomaticallySyncScene = true;
        BattleButton.SetActive(true);
    }
 

    public void OnBattleButtonClicked()
    {
        Debug.Log("Battle button was clicked");
        BattleButton.SetActive(false);
        CancelButton.SetActive(true);
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("Tried to join a random game, but failed.There must be no open games aviable");
        CreateRoom();
    }

    void CreateRoom()
    {
        
        int RandomRoomName = Random.Range(0, 10000);
        
        RoomOptions RoomOps = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = (byte)MultiplayerSetting.multiplayerSetting.MaxPlayers };
        PhotonNetwork.CreateRoom("Room" + RandomRoomName, RoomOps);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Tried to create a new room but filed, there be already be a room whith the same name");
        CreateRoom();
    }


    public void OnCancelButtonClicked()
    {
        CancelButton.SetActive(false);
        BattleButton.SetActive(true);
        PhotonNetwork.LeaveLobby();
    }
}
